# Nest Sensor for Raspberry Pi

This script will eventually work with temperature sensors built into a Raspberry Pi, in order to provide additional room data to the Nest in order to wider regulate temperatures.

For instance, if the house is set to be cooled to 78, but the bedroom is currently occupied and the bedroom is being reported by this sensor to be 80, then cool off the house more than the Nest temperature is set to.

# How to Install

* Download and run `npm install` from the project root.
* Add `.env` file with your Nest Username/Password and Nest Thermostat Serial Number
* Run `node nest.js` from the command line.


Example `.env` file
```
NEST_USER=example@example.com
NEST_PASSWORD=boogiewoogie
NEST_SERIAL=XXXXXXXXXXXXXXXX
```

Your Nest Thermostat's serial number can be found by logging onto [Nest](http://home.nest.com), or accessing your physical Nest and looking at the "Technical Information" for the Thermostat you want to add the sensor to.

# Changelog

### 0.2.0

* Currently only runs once, and shows Current Temperature and Goal Temperature of the Nest proper, provided you manually enter Nest serial number.

# Roadmap

### Software

* Modify to be run as a cron.
* Log out the data
* Configure to send Emails at regular intervals to NEST_USER?
* Configure RaspPi to keep script in crontab, and keep RaspPi online
* Track stats?

### Hardware

* Configure a Raspberry Pi with Temperature Sensor
* Provide guidelines for how to install and build a Raspberry Pi with the appropriate Temperature sensor.

# Contributors

* [Chris Lagasse](http://chrislagasse.com)