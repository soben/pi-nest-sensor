
function displayTemperature(temperature, scale) {
  var formatted = 0;
  
  // This person likes Farenheit.
  if ( 'C' == scale ) {
    formatted = Math.round(temperature);
  } 
  else {
    // Convert to Fahrenheit
    formatted = Math.round((temperature * (9 / 5.0)) + 32.0);
  }

  return formatted + 'º' + scale;

};


// DATA IS IN CELCIUS.

var nestTemperature;
var nestTargetTemperature;
var nestTemperatureScale;

var sensorTemperature;

var dht = require('dht-sensor');
var gpio = require('pi-gpio');

var Sensor = function Constructor(nestAPIObject, nestSerial, thermostatGPIO, debugMode) {
    this.debugMode = debugMode;
    this.nestAPI = nestAPIObject;
    this.nestSerialID = nestSerial;

    this.gpioID = thermostatGPIO;

    // @todo Confirm functionality?
};

Sensor.prototype.check = function() {
  this.updateSensorData();
  this.updateNestData();
}

Sensor.prototype.updateSensorData = function () {
  sensorRoomStatus = false; // Detects the prescence of a human?
  sensorTemperature = 21;

  // @todo Make this work with ANY dhtSensor?
  var currentDHTdata = dht.read(11, this.gpioID); // dhtSensor ID, GPIO ID

  sensorTemperature = currentDHTdata.temperature;
}

Sensor.prototype.updateNestData = function () {
  return this.nestAPI.getInfo(this.nestSerialID, function (nestData) {
    nestTemperatureScale = nestData.temperature_scale;
    nestTemperature = nestData.current_temperature;
    nestTargetTemperature = nestData.target_temperature;
  });
}

Sensor.prototype.logData = function () {
  if ( this.debugMode ) {
    console.log('Nest Temperature Scale: ' + nestTemperatureScale);
    console.log('Current Temperature is: ' + displayTemperature(nestTemperature, nestTemperatureScale));
    console.log('Target Temperature is: ' + displayTemperature(nestTargetTemperature, nestTemperatureScale));

    console.log('Sensor Temperature is: ' + displayTemperature(sensorTemperature, 'C'));
  }
}

module.exports = Sensor;