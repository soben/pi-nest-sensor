require('dotenv').config({silent: true});

var NestSensorHandler = require('./library/sensorHandler');

// Let's make sure we have all of the variables we need.
var requireEnv = require("require-environment-variables");
requireEnv(['NEST_USER', 'NEST_PASSWORD', 'NEST_SERIAL', 'THERMOSTAT_GPIO_ID']);

var debug = false;
if ( process.env.DEBUG == 'true' ) {
  debug = true;
}

var nest = require('nest-thermostat').init(process.env.NEST_USER, process.env.NEST_PASSWORD);
var sensor = new NestSensorHandler(nest, process.env.NEST_SERIAL, process.env.THERMOSTAT_GPIO_ID, debug);

// All of the watch actions, and what they need to do.

// Every 1 minute
  // trigger the sensor.
  sensor.check();

  setTimeout(function () {
    sensor.logData();
  }, 2000);
